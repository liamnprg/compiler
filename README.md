# Compiler

This is a simple compiler for a langauge I made up called alang. Look at the sample.al file to get a gist of the language. It is quite simple.

This code requires ir-shared cloned to the top of this repository

## Deps

ld linker, llvm installation, running `git clone git@gitlab.com:liamnprg/ir-shared.git`

## Usage

run `cargo run` to get how the program works, it's pretty simple


# Todo

- implement std::display for Cerror, making error messages better.
- Stop using unwraps and panics where not neccissary in lib.rs

