#![allow(unused_assignments)]
use futures::prelude::*;
use crate::*;
pub struct Lexer {
    pub chars: Vec<u8>,
    pub pos: usize,
    pub tokens: Vec<Token>,
    pub lno: u32,
}

impl futures::Stream for Lexer {
    type Item = Token;
    type Error=();
    fn poll(&mut self) -> Poll<Option<Self::Item>,Self::Error> {
        let cu = self.chars.get(self.pos);
        let c: char;
        if cu == None {
            return Ok(Async::Ready(Some(Token::EOF)))
        } else {
            c = *cu.unwrap() as char;
        }
        //println!("print char: {}", c);
        let mut ret = Ok(Async::NotReady);
        if c == '=' {
            ret = Ok(Async::Ready(Some(Token::Equ)));
        } else if c.is_alphabetic() {
            ret = Ok(Async::Ready(Some(Token::Ident(Ident::Reg(RegV{r:c})))));
        } else if c.is_numeric() {
            let mut buf = String::new();
            buf.push(c);
            loop {
                self.pos+=1;
                let zu = self.chars.get(self.pos);
                let z: char;
                if zu == None {
                    return Ok(Async::Ready(Some(Token::EOF)))
                } else {
                    z = *zu.unwrap() as char;
                }
                if z.is_numeric() {
                    buf.push(z);
                } else {
                    self.pos-=1;
                    ret = Ok(Async::Ready(Some(Token::Ident(Ident::Liter(buf.parse().unwrap())))));
                    break;
                }
            }
        } else if c == '*' || c == '/' || c == '-' || c == '+' {
            let r = match c {
                '*' => Oper::Mul,
                '-' => Oper::Sub,
                '/' => Oper::Div,
                '+' => Oper::Plus,
                _ => panic!(),
            };
            ret =  Ok(Async::Ready(Some(Token::Oper(r))));
        } else if c == '\n' {
            self.lno+=1;
            ret = Ok(Async::Ready(Some(Token::Lno(self.lno))));
        } else {
            ret = Ok(Async::Ready(Some(Token::Idk(c))));
        }
        self.pos+=1;
        return ret;
    }
}
