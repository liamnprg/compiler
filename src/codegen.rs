extern crate llvm_sys as llvm;

use llvm_sys::target::{LLVM_InitializeNativeTarget,LLVM_InitializeNativeAsmPrinter};
use std::ffi::CString;
use crate::Ident::{Reg,Liter};
use std::fs;
use crate::*;
use std::ptr;
use llvm::prelude::*;
use llvm::core::*;
use llvm_sys::target_machine::*;
use llvm_sys::bit_reader::LLVMParseBitcode2;
use llvm_sys::linker::LLVMLinkModules2;
use std::process::Command;
use llvm_sys::target::LLVM_InitializeAllTargets;
use llvm_sys::target::LLVM_InitializeAllAsmPrinters;
use llvm_sys::target_machine::LLVMCodeGenOptLevel::LLVMCodeGenLevelAggressive;
use llvm_sys::target_machine::LLVMRelocMode::LLVMRelocDefault;
use llvm_sys::target_machine::LLVMCodeModel::LLVMCodeModelDefault;
use llvm_sys::target_machine::LLVMCodeGenFileType::LLVMObjectFile;

//The a-e are the registers
struct Gen {
    ast:Ast,
    bld:LLVMBuilderRef,
    a:LLVMValueRef,
    b:LLVMValueRef,
    c:LLVMValueRef,
    d:LLVMValueRef,
    e:LLVMValueRef,
}

//Converts an i8 to a CString. 
fn t(i:i8) -> CString {
     CString::new(format!("t{}",i.to_string())).unwrap()
}

impl Gen {
    fn reg2val(&self,r:RegV) -> LLVMValueRef{
        match r.r {
            'a' => self.a,
            'b' => self.b,
            'c' => self.c,
            'd' => self.d,
            'e' => self.e,
            _ => panic!("Used a register other than a-e"),
        }
    }
    fn codegen(self) {
        unsafe {
        let mut tcount:i8 = 0;
        let ty = LLVMInt32Type();
        for e in &self.ast.e {
            let a1 = e.arg1;
            let a2 = e.arg2;
            let dst = e.dst;
            let o = e.oper;
            let mut tmp1: LLVMValueRef = self.a;
            let mut tmp2: LLVMValueRef = self.b;
            //depending on the values of the statement, we will need to construct different
            //assortments of loads,stores and operations
            if let Reg(r) = a1 {
                
                tmp1 = LLVMBuildLoad(self.bld,self.reg2val(r),t(tcount).as_ptr());
                tcount+=1;

            } else if let Liter(i) = a1 {
                tmp1 = LLVMConstInt(ty,i as u64 ,0);
            }
            if let Reg(r) = a2 {
                tmp2 = LLVMBuildLoad(self.bld,self.reg2val(r),t(tcount).as_ptr());
                tcount+=1;
            } else if let Liter(i) = a2 {
                tmp2 = LLVMConstInt(ty,i as u64,0);
            }
            let res = match o {
                Oper::Plus => LLVMBuildAdd(self.bld,tmp1,tmp2,t(tcount).as_ptr()),
                Oper::Sub =>LLVMBuildSub(self.bld,tmp1,tmp2,t(tcount).as_ptr()) ,
                Oper::Div => LLVMBuildUDiv(self.bld,tmp1,tmp2,t(tcount).as_ptr()),
                Oper::Mul => LLVMBuildMul(self.bld,tmp1,tmp2,t(tcount).as_ptr()),
            };
            LLVMBuildStore(self.bld,res,self.reg2val(dst));
            tcount+=1;
        }
    }
    }
}

pub fn codegen(ast:Ast,triple:String) {
        unsafe {
        // Set up a context, module and builder in that context.
        let context = llvm::core::LLVMContextCreate();
        let module = llvm::core::LLVMModuleCreateWithNameInContext(b"alang\0".as_ptr() as *const _,context);
        let builder = llvm::core::LLVMCreateBuilderInContext(context);

        // Get the type signature for void nop(void);
        // Then create it in our module.
        let void = llvm::core::LLVMVoidTypeInContext(context);
        let function_type = llvm::core::LLVMFunctionType(void, ptr::null_mut(), 0, 0);
        let function = llvm::core::LLVMAddFunction(module, b"main\0".as_ptr() as *const _, function_type);

        // Create a basic block in the function and set our builder to generate
        // code in it.
        let bb = llvm::core::LLVMAppendBasicBlockInContext(context,
                                                           function,
                                                           b"entry\0".as_ptr() as *const _);
        llvm::core::LLVMPositionBuilderAtEnd(builder, bb);

        //module name
        let nme =CString::new("libutil").unwrap();
        //The path to libutil.bc, a library containing a function we need
        let b = fs::read(&"ir-shared/libutil.bc").unwrap();
        let len = b.len();


        //load the module
        let buf = LLVMCreateMemoryBufferWithMemoryRange(b.as_ptr() as *const _,len,nme.as_ptr(),1);

        let mut libnative = LLVMModuleCreateWithNameInContext(nme.as_ptr() as *const _,context);
        LLVMParseBitcode2(buf,&mut libnative as *mut *mut _);
        LLVMLinkModules2(module,libnative);
        //this is the function we need, it creates a summary of done operations
        let print_regs = LLVMGetNamedFunction(module, CString::new("print_regs").unwrap().as_ptr() as *const _);


        //function prologue
        let ty = LLVMInt32Type();

        //allocate registers 
        let a = LLVMBuildAlloca(builder,ty,b"a\0".as_ptr() as *const _);
        let b =LLVMBuildAlloca(builder,ty,b"b\0".as_ptr() as *const _);
        let c =LLVMBuildAlloca(builder,ty,b"c\0".as_ptr() as *const _);
        let d =LLVMBuildAlloca(builder,ty,b"d\0".as_ptr() as *const _);
        let e =LLVMBuildAlloca(builder,ty,b"e\0".as_ptr() as *const _);

        //function body
        let gen = Gen {
            ast:ast,
            bld:builder,
            a:a,
            b:b,
            c:c,
            d:d,
            e:e,
        };
        //This creates the body of the function
        gen.codegen();
        /*


        }
        */

        //function end/prologue
        let la = LLVMBuildLoad(builder,a,b"la\0".as_ptr() as *const _);
        let lb = LLVMBuildLoad(builder,b,b"lb\0".as_ptr() as *const _);
        let lc = LLVMBuildLoad(builder,c,b"lc\0".as_ptr() as *const _);
        let ld = LLVMBuildLoad(builder,d,b"ld\0".as_ptr() as *const _);
        let le = LLVMBuildLoad(builder,e,b"le\0".as_ptr() as *const _);
        let mut args = [la,lb,lc,ld,le];
        //This calls the print_regs function, which does what it says on the tin
        LLVMBuildCall(builder,print_regs,args.as_mut_ptr(),args.len() as u32, CString::new("").unwrap().as_ptr() as *const _);

        // Emit a `ret void` into the function
        LLVMBuildRetVoid(builder);


        LLVMDumpModule(module);
        genasm(module,triple);

        





        // Clean up. Values created in the context mostly get cleaned up there.
        llvm::core::LLVMDisposeBuilder(builder);
        llvm::core::LLVMDisposeModule(module);
        llvm::core::LLVMContextDispose(context);
    }
}
fn genasm(module:LLVMModuleRef, triple:String) {
    unsafe {
//When does what have to be initialized
    //needed for getdefaulttargettriple to work, and getfirsttarget
        if triple == "" {
        LLVM_InitializeNativeTarget();
        let hcpu = LLVMGetHostCPUName();
        let dtriple = LLVMGetDefaultTargetTriple();
        let mut features = LLVMGetHostCPUFeatures();
        let tgt = LLVMGetFirstTarget();

        let machine_target = LLVMCreateTargetMachine(
            tgt,
            dtriple,
            hcpu,
            features,
            LLVMCodeGenLevelAggressive,
            LLVMRelocDefault,
            LLVMCodeModelDefault);

        LLVMDisposeMessage(hcpu);
        LLVMDisposeMessage(features);
        LLVMDisposeMessage(dtriple);
        let ptr = &mut b"".as_ptr() as *mut _;
        LLVM_InitializeNativeAsmPrinter();
        printptr(LLVMTargetMachineEmitToFile(machine_target,
                                    module,
                                    CString::new("/tmp/file.o").unwrap().as_ptr() as *mut _,
                                    LLVMObjectFile,
                                    ptr as *mut *mut _),ptr as *mut *mut _);
        } else {
            panic!("Using other targets is not supported at the moment");
        }
        /* else {

            let mut ctriple = CString::new(triple.clone().into_bytes()).unwrap();
            let mut tgt = LLVMGetFirstTarget();
            //segfaults here because name is a property of the target struct, so it returns
            //nothing=segfault
//            let mut cpu = LLVMGetTargetName(tgt);
            let mut cpu = CString::new("").unwrap();
        let ptr = &mut b"".as_ptr() as *mut _;
        //needed for the following line 
            LLVM_InitializeAllTargets();
            LLVM_InitializeNativeTarget();
            printptr(LLVMGetTargetFromTriple(ctriple.as_ptr(),&mut tgt as *mut *mut _, ptr as *mut *mut _),ptr as *mut *mut _);

            LLVM_InitializeAllAsmPrinters();
            LLVM_InitializeNativeAsmPrinter();
            let machine_target = LLVMCreateTargetMachine(
                tgt,
                ctriple.as_ptr(),
                cpu.as_ptr(),
                CString::new("").unwrap().as_ptr(),
                LLVMCodeGenLevelAggressive,
                LLVMRelocDefault,
                LLVMCodeModelDefault);
            printptr(LLVMTargetMachineEmitToFile(machine_target,
                                        module,
                                        CString::new("/tmp/file.o").unwrap().as_ptr() as *mut _,
                                        LLVMObjectFile,
                                        ptr as *mut *mut _),ptr as *mut *mut _);
        }*/
    
         println!("{:?}",Command::new("ld")
            .arg("-o")
            .arg("./a.out")
            .arg("/tmp/file.o")
            .arg("ir-shared/libx86_64.o")
            .output()
            .expect("failed to execute process"));
    }
}
//This function does conditional error checking
fn printptr(i:LLVMBool,ptr: *mut *mut i8) {
    if i == 1 {
        unsafe {
            println!("{:?}",CString::from_raw(*ptr));
        }
    }
}






















        /*
        LLVM_InitializeNativeTarget();
        let tgt = LLVMGetTargetFromName(CString::new("x86-64").unwrap().as_ptr() as *const _);
        let msg = CString::new(triple.clone().into_bytes()).unwrap();
        let mut ptr = msg.as_ptr() as *mut _;
        if &triple != "" {
            println!("Not using default triple {}",LLVMGetTargetFromTriple(msg.as_ptr() as *mut _,tgt as *mut _,ptr ));
        } 
//        let msg = CString::new("x86_64-pc-linux-gnu").unwrap();


        //printptr(LLVMGetTargetFromTriple( msg ,tgt as *mut _,ptr as *mut *mut _),ptr);
//        let cpu = LLVMGetTargetMachineCPU(tgt);
        
        LLVM_InitializeAllTargets();
        LLVM_InitializeAllAsmPrinters();



        let features = CString::new("").unwrap();
        let cpu = CString::new("generic").unwrap();
        let mtgt = LLVMCreateTargetMachine(tgt,msg.as_ptr() as *mut _,cpu.as_ptr(),features.as_ptr(),LLVMCodeGenOptLevel::LLVMCodeGenLevelAggressive,LLVMRelocMode::LLVMRelocDefault,LLVMCodeModel::LLVMCodeModelDefault);



        println!("Created target!I!");
        let file=CString::new("/tmp/file.o").unwrap();
        printptr(LLVMTargetMachineEmitToFile(mtgt,module,file.as_ptr() as *mut _,LLVMCodeGenFileType::LLVMObjectFile,ptr),ptr);
        */
