#![feature(futures_api)]
#![feature(type_ascription)]
use futures::prelude::*;
use std::fmt;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::sync::mpsc::channel;
use futures::Stream;
use crate::parse::parse_tokens;
use crate::lex::Lexer;

mod parse;
mod lex;
pub mod codegen;

#[derive(Debug,PartialEq,Eq,Clone)]
pub struct Ast {
    e: Vec<Expr>,
}
impl fmt::Display for Ast {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //write!(f, "Error: {}; on line {}", self.lno, self.msg)
        for e in &self.e {
            writeln!(f, "{:?}={:?} {:?} {:?}",e.dst,e.arg1,e.oper,e.arg2)?;
        }
        Ok(())
    }
}

#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub struct Expr {
    dst: RegV,
    arg1: Ident,
    arg2: Ident,
    oper: Oper,
}
#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub enum Oper {
    Plus,Sub,Div,Mul
}

#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub struct RegV {
    pub r:char,
}

#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub enum Ident {
    Reg(RegV),Liter(i32),
}

#[derive(Debug,PartialEq,Eq)]
pub struct Cerror {
    lno: u32,
    msg: String,
}
impl fmt::Display for Cerror {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error: {}; on line {}", self.lno, self.msg)
    }
}
impl Cerror {
    fn new(lno:u32,msg:&str) -> Self {
        Cerror { lno:lno,msg:msg.to_string()}
    }
}

#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub enum Token {
    Ident(Ident),Oper(Oper),Equ,EOF,Idk(char),Lno(u32)
}


pub fn lex(s:String) -> Result<Ast,Cerror> {
    let mut lxr = Lexer { chars: s.into_bytes(), pos:0,tokens:vec!(), lno:1};

    let (tx, rx) = channel();
	let finall = Arc::new(Mutex::new(Ast {e:vec!()}));
    let final_ast = finall.clone();
    


	//let mut finalAst = Mutex::new(Ast {e:vec!()});
    let thread = thread::spawn(move|| {
		let mast = parse_tokens(rx);
        if mast.is_ok() {
            *final_ast.lock().unwrap() = mast.unwrap();
            return true;
        } else {
            return false;
        }
	});
    loop {
        let r = lxr.poll();
		if let Ok(Async::Ready(Some(t))) = r {
			tx.send(t).unwrap();
            if t == Token::EOF {
                break;
            }
		}
    }
    let ok = thread.join().unwrap();
    if !ok {
        panic!("encountered error");
    }
    let nast = finall.lock().unwrap().clone();
    //unimplemented!();
   Ok(nast)
}
