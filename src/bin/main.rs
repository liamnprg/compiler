#[macro_use]
extern crate structopt;

use std::path::PathBuf;
use structopt::StructOpt;
use alang::lex;
use alang::codegen::codegen;
use std::fs::File;
use std::io::prelude::*;

#[derive(StructOpt, Debug)]
#[structopt(raw(setting = "structopt::clap::AppSettings::ColoredHelp"))]
struct Opt {
    ///Pass a target triple, otherwise the default is the native triple. You must have a linker for
    ///this target installed
    #[structopt(short = "t",long = "target",default_value="")]
    target: String,
    ///Enable extra debug info
    #[structopt(short = "d")]
    debug: bool,
    ///The file to compile, the suffix should be .al, but this is not required.
    file: String,
}

fn main() {
    let opt = Opt::from_args();
//    println!("{:?}", opt);
    let mut file = File::open(opt.file).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    let ast = lex(contents).unwrap();
    codegen(ast,opt.target);
}

