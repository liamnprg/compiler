use crate::*;
use std::sync::mpsc::Receiver;

struct Parser {
    rx:Receiver<Token>,
    ast:Ast,
    buftoken: Option<Token>,
    lno: u32,

}
impl Parser {
    fn gettoken(&mut self) -> Token {
        if let Some(t) = self.buftoken {
            //println!("{:?}",t);
            self.buftoken= None;
            return t;
        }
        loop {
            let t = self.rx.recv().unwrap();
            //println!("{:?}",t);
            if let Token::Lno(l) = t {
                self.lno = l;
             //   println!("{:?}",t);
                continue;
            }
            return t;
        }
    }
    fn bt(&mut self, t:Token) {
        self.buftoken=Some(t);
    }
}
pub fn parse_tokens(rx: Receiver<Token>) -> Result<Ast,Cerror> {
    let myast = Ast { e:vec!()};
    let mut par = Parser{rx:rx,ast:myast,buftoken:None,lno:1};

    loop {
        parse_expr(&mut par).unwrap();
        let t = par.gettoken();
        if &t == &Token::EOF {
            //println!("Finished:\n {}", par.ast);
            break;
        } else {
            par.bt(t);
        }
    }
    Ok(par.ast)
} 

fn parse_expr(par: &mut Parser) -> Result<(),Cerror> {
    let t = par.gettoken();
    let dst = match t {
        Token::Ident(Ident::Reg(t)) => t,
        t => return Err(Cerror::new(par.lno,&format!("was expecting a register but found {:?}",t))),
    };
    let t = par.gettoken();
    match t {
        Token::Equ => t,
        _ => return Err(Cerror::new(par.lno,&format!("was expecting an equals sign but found {:?}",t))),
    };
    let t = par.gettoken();

    let arg1 = match t {
        Token::Ident(t) => t,
        t => return Err(Cerror::new(par.lno,&format!("was expecting a register/value but found {:?}",t))),
    };

    let t = par.gettoken();
    let op = match t {
        Token::Oper(t) => t,
        _ => return Err(Cerror::new(par.lno,&format!("was expecting an operator like +,-,/, or *, but found {:?}",t))),
    };
    let t = par.gettoken();

    let arg2 = match t {
        Token::Ident(t) => t,
        t => return Err(Cerror::new(par.lno,&format!("was expecting a register/value but found {:?}",t))),
    };
    par.ast.e.push(Expr {dst:dst,arg1:arg1,arg2:arg2,oper:op});


    Ok(())
}
